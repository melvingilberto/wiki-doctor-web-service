@extends('app')

@section('content')
    <div class="jumbotron text-center">
	  <h1>Wiki Doctor</h1>
	</div>

	<div class="container">

		<h2 class="text-center">
			¿Qué es Wiki Doctor?
		</h2>
		<p>
			El objetivo de wiki doctor es brindar a diferentes usuarios la posibilidad de consultar diferentes síntomas en las partes del cuerpo y para ello brinda un servicio web por el cual se pueden consultar estos servicios, retornando toda la información en un formato JSON.
		</p>

		<h2 class="text-center">
			API
		</h2>
		<button class="btn btn-primary center-block text-center" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
		  Estructura JSON
		</button>
		<br>
		<div class="collapse" id="collapseExample">
	    <pre>
{
  "error": false,
  "message": "Success",
  "data": [
    {
     // Informaction json
    }
  ]
}
	    </pre>	
		</div>
		<p>
			<b>Partes del cuerpo: </b>Para hacer la petición a nuestros servicios sobre las partes del cuerpo debes hacer la petición a la URL de nuestro dominio:
			<div class="bs-callout bs-callout-danger"> <h4>URL: </h4> /body-parts</div>

		</p>
		<p>
			<b>Síntomas: </b>Los síntomas están relacionados a una parte del cuerpo, por lo cual necesitas pasar el ID de la parte del cuerpo:
			<div class="bs-callout bs-callout-danger"> <h4>URL: </h4> /symptoms/{id}</div>

		</p>
		<p>
			<b>Conceptos: </b>para poder determinar las causas de un síntoma, concepto y como poder tratarlo debes de pasar el ID del síntoma.
			<div class="bs-callout bs-callout-danger"> <h4>URL: </h4> /concepts/{id}</div>

		</p>

	</div>
@stop