<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BodyParts extends Model {

	protected $fillable = ['body_parts_name'];

}
