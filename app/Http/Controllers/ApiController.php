<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
class ApiController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home');
	}

	public function bodyParts(){
		try {
			$query = DB::table('body_parts')->select('id','body_parts_name')->get();
			return response()->json(['error' => false,'message' => 'Success', 'data' => $query]);
		} catch (Exception $e) {
			return response()->json(['error' => true, 'message' => "Error, try again", 'data' => []]);
		}
	}

	public function symptoms($id){
		try {
			$query = DB::table('symptoms')->select('id','symptoms_name','body_parts_id')->where('body_parts_id', $id)->get();
			return response()->json(['error' => false,'message' => 'Success', 'data' => $query]);
		} catch (Exception $e) {
			return response()->json(['error' => true, 'message' => "Error, try again", 'data' => []]);
		}
	}
	public function concepts($id){
		try {
			$query = DB::table('concepts')->select('id','concepts_description','image','symptoms_id')->where('symptoms_id', $id)->get();
			return response()->json(['error' => false,'message' => 'Success', 'data' => $query]);
		} catch (Exception $e) {
			return response()->json(['error' => true, 'message' => "Error, try again", 'data' => []]);
		}
	}

}
