<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Symptoms extends Model {

	protected $fillable = ['symptoms_name','body_parts_id'];
}
