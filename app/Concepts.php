<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Concepts extends Model {

	protected $fillable = ['concepts_description','symptoms_id','image'];

}
