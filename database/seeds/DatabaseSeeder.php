<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\BodyParts;
use App\Symptoms;
use App\Concepts;


class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();


		$this->call('DataBodyParts');
		$this->call('DataSymptoms');
		$this->call('DataConcepts');
	}

}



class DataBodyParts extends Seeder {

    public function run()
    {
        DB::table('body_parts')->delete();

        BodyParts::create(array(
        	'body_parts_name' => 'Cabeza',
    	));

    	BodyParts::create(array(
        	'body_parts_name' => 'Garganta',
    	));

    	BodyParts::create(array(
        	'body_parts_name' => 'Torax y abdomen',
    	));
    	BodyParts::create(array(
        	'body_parts_name' => 'Brazos',
    	));

    	BodyParts::create(array(
        	'body_parts_name' => 'Zona pélvica',
    	));
    	BodyParts::create(array(
        	'body_parts_name' => 'Piernas',
    	));
    	BodyParts::create(array(
        	'body_parts_name' => 'Pies',
    	));
    }

}


class DataSymptoms extends Seeder {

    public function run()
    {
        DB::table('symptoms')->delete();

        Symptoms::create(array(
        	'symptoms_name' => 'Afasia (pérdida del lenguaje)',
        	'body_parts_id' => 1,
    	));
        Symptoms::create(array(
        	'symptoms_name' => 'Caída de cabello (alopecia)',
        	'body_parts_id' => 1,
    	));

    	
    }

}


class DataConcepts extends Seeder {

    public function run()
    {
        DB::table('concepts')->delete();

        Concepts::create(array(
        	'concepts_description' => '
				La afasia es la pérdida de la capacidad de comprender o emitir el lenguaje. Es un trastorno adquirido que se produce a consecuencia de una lesión en una región determinada del cerebro. Supone la pérdida total o parcial del lenguaje adquirido previamente.

				En primer lugar, son responsables de la afasia los trastornos circulatorios de los vasos sanguíneos de la zona del cerebro responsable del funcionamiento del lenguaje (apoplejía o infarto cerebral). También está asociada a problemas cardiovasculares como arteriosclerosis, diabetes mellitus, hipertensión arterial, hipercolesterolemia o consumo de tabaco. Además, las hemorragias y los tumores pueden causar lesiones cerebrales con el consiguiente trastorno del habla. La afasia infantil se produce normalmente tras un traumatismo craneoencefálico. Durante un infarto cerebral (ictus) una parte del cerebro deja de recibir suficiente oxígeno, lo que puede llevar a la muerte del tejido cerebral.
        	',
        	'symptoms_id' => 1,
    	));
        Concepts::create(array(
        	'concepts_description' => '
        		La alopecia es una pérdida excesiva de pelo en la que pueden llegar a caerse más de 120 cabellos al día, y revela un visible debilitamiento del cabello, incluso una calvicie evidente. La caída del cabello puede aparecer tanto en hombres como en mujeres y puede tener diferentes causas. En la mayoría de los casos, se trata de una predisposición genética, que hace que los folículos capilares reaccionen de forma más sensible a las hormonas sexuales.

				Todas las personas perdemos al día de 50 a 100 cabellos de forma natural. Una pérdida superior a 120 cabellos cada día, no obstante, puede considerarse un principio de alopecia. La llamada alopecia androgénica (alopecia genéticamente heredada) afecta a uno de cada dos hombres a lo largo de su vida. La pérdida de cabello puede ser leve o afectar a toda la cabeza. La alopecia androgénica también es la forma más común de alopecia entre las mujeres. Se estima que la padece una de cada cinco mujeres. No obstante, en las mujeres la alopecia determinada genéticamente tiene unos síntomas diferentes: el cabello se pierde de forma difusa, no tan localizada como ',
        	'symptoms_id' => 2,
    	));

    	
    }

}
